const code = require('code') // assertion library
const Lab = require('lab')
const server = require('../index.js')
const lab = exports.lab = Lab.script()

lab.experiment('Routes', function () {
  lab.test("There is a non-null response on GET-route '/'",
    function (done) {
      var options = {method: 'GET', url: '/'}
      server.inject(options, function (response) {
        var result = response.result
        code.expect(result).to.exist()
        done()
      })
    })
})
