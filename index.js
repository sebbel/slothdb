const hapi = require('hapi')
const server = new hapi.Server()

server.connection({'port': 5000})

server.register([require('inert'), require('vision')], () => {

  server.views({
    engines: {
      hbs: require('handlebars')
    },
    relativeTo: __dirname,
    path: './views',
    layoutPath: './views/layout',
    layout: true,
    isCached: false,
    helpersPath: './views/helpers',
    partialsPath: './views/partials'
  })

  server.route(require('./routes'))

  server.start(() => {
    console.log('Sloth Database running at port 5000!')
  })
})
module.exports = server
