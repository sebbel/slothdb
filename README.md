# SlothDB 
![CircleCI Badge](https://circleci.com/gh/sebbel/slothdb/tree/master.svg?style=shield&circle-token=:circle-token)

Just your friendly sloth directory.

# Current stack
- `nodejs`
- `hapijs`
- `handlebars`

# How to run?

1. clone this repo

```
git clone https://gitlab.com/sebbel/slothdb
```

2. install dependencies

```
npm install
```

3. run!

```
npm start
```

Your Sloth Database should now be up on port 5000.

# TODO

- Sloth CRUD
- Connection to database
- OAUTH (github?)